// [SECTION] Dependencies and Modules
	const User = require('../models/User');
	const Course = require('../models/Course');
	const bcrypt = require('bcrypt');
	const auth = require('../auth');

// [SECTION] Function - Create
	// Create a User
		module.exports.registerUser = (info) => {
			let fName = info.firstName;
			let lName = info.lastName;
			let email = info.email;
			let passW = info.password;
			let mobil = info.mobileNo;
			
			let newUser = new User({
		  		firstName: fName,
		  		lastName: lName,
		  		email: email,
		  		password: bcrypt.hashSync(passW, 10),
		  		mobileNo: mobil
		  	});

			return newUser.save().then((user, error) => {
				if (user) {
					return user;
				} else {
					return false; 		
				}
			});
		}

	// Email Checker
		module.exports.checkEmailExists = (reqBody) => {
			return User.find({email: reqBody.email}).then(result => {
				if (result.length > 0) {
					return 'Email Already Exists.';
				} else {
					return 'Email is Still Available.';
				}
			});
		}

	// Login (User Authentication)
		module.exports.loginUser = (reqBody) => {
			return User.findOne({email: reqBody.email}).then(result => {
				let uPassW = reqBody.password;
				if (result === null) {
					return false;
				} else {
					const isMatched = bcrypt.compareSync(uPassW, result.password);
					if (isMatched) {
						let userData = result.toObject();
						return {access: auth.createAccessToken(userData)};
					} else {
						return false;
					}
				}
			});
		}

	// Enroll (Course Enrollment)
		module.exports.enroll = async (data) => {
			let id = data.userId;
			let course = data.courseId;

			let isUserUpdated = await User.findById(id).then(user => {
				let courseFound = user.enrollments.find(courseObj => courseObj.courseId == course);
				if (!courseFound){
					user.enrollments.push({courseId: course});
					return user.save().then((save, error) => {
						if (error) {
							return false;
						} else {
							return true;
						}
					});
				} else {
					return false;
				}
			});

			let isCourseUpdated = await Course.findById(course).then(course => {
				let userFound = course.enrollees.find(courseObj => courseObj.userId == id);
				if (!userFound){
					course.enrollees.push({userId: id});
					return course.save().then((saved, err) => {
						if (err) {
							return false;
						} else {
							return true;
						}
					});
				} else {
					return false;
				}	
			});

			if (isUserUpdated && isCourseUpdated) {
				return 'Enrollment Successful.';
			} else {
				return 'Enrollment Failed.';
			}
		}
		

// [SECTION] Function - Retrieve
	// User Profile
		module.exports.getProfile = (id) => {
			return User.findById(id).then(result => {
				return result;
			});
		}

// [SECTION] Function - Update
	// Set User as Admin (isAdmin = true)
		module.exports.setAsAdmin = (userId) => {
			let updates = {
				isAdmin: true
			}
			return User.findByIdAndUpdate(userId,updates).then((admin, err) => {
				if (admin) {
					return 'Role has been changed to Admin.';
				} else {
					return 'Failed to implement update.'; 		
				}
			});
		}

	// Set User as Non-Admin (isAdmin = false)
		module.exports.setAsNonAdmin = (userId) => {
			let updates = {
				isAdmin: false
			}
			return User.findByIdAndUpdate(userId,updates).then((user, err) => {
				if (user) {
					return 'Role has been changed to non-admin.';
				} else {
					return 'Failed to implement update.'; 		
				}
			});
		}

// [SECTION] Function - Delete