// [SECTION] Dependencies and Modules
	const express = require('express');
	const mongoose = require("mongoose");
	const dotenv = require("dotenv");
	const cors = require("cors");
	const courseRoutes = require('./routes/courses');
	const userRoutes = require('./routes/users');

// [SECTION] Environment Variables Setup
	dotenv.config();
	const port = process.env.PORT;
	const mongoDb = process.env.MONGO_URL;

// [SECTION] Server Setup
	const app = express();
	app.use(express.json());
	app.use(cors());
	app.use(express.urlencoded({extended: true}));

// [SECTION] Database Connect
	mongoose.connect(mongoDb);
	const dbStatus = mongoose.connection;
	dbStatus.once('open', () => console.log('Connected to MongoDB Atlas.'));

// [SECTION] Server Routes
	app.use('/courses',courseRoutes);
	app.use('/users',userRoutes);

// [SECTION] Server Responses
	app.get('/', (req, res) => {
		res.send(`Project deployed succussfully.`)
	})
	app.listen(port, () => {
   		console.log(`API is now online on port ${port}`);
	});
