// [SECTION] Dependencies and Modules
	const mongoose = require('mongoose');

// [SECTION] Schema
	const userSchema = new mongoose.Schema({
		firstName: {
			type: String,
			required: [true, 'First name is Required.']
		},
		lastName: {
			type: String,
			required: [true, 'Last name is Required.']
		},
		email: {
			type: String,
			required: [true, 'Email is Required.']
		},
		password: {
			type: String,
			required: [true, 'Password is Required.']
		},
		mobileNo: {
			type: String,
			required: [true, 'Mobile number is Required.']
		},
		isAdmin: {
			type: Boolean,
			default: false 
		},
		enrollments: [
			{
				courseId: {
					type: String,
					required: [true, 'Course ID is Required.']
				},
				enrolledOn: {
					type: Date,
					default: new Date()
				},
				status: {
					type: String,
					default: 'Enrolled'
				}
			}
		]
	})

// [SECTION] Model
	module.exports = mongoose.model("User", userSchema);