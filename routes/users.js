// [SECTION] Dependencies and Modules
	const exp = require('express');
	const controller = require('../controllers/users');
	const auth = require('../auth');

// [SECTION] Routing Component
	const route = exp.Router();

// [SECTION] Route - Create *POST*
	// Create User
		route.post('/register', (req, res) => {
			let userInfo = req.body;
			controller.registerUser(userInfo).then(outcome => {
				res.send(outcome);
			});
		});
	// Check Email
		route.post('/check-email', (req, res) => {
			controller.checkEmailExists(req.body).then(outcome => {
				res.send(outcome);
			});
		});
	// Login User
		route.post('/login', (req, res) => {
			let data = req.body;
			controller.loginUser(data).then(outcome => {
				res.send(outcome);
			});
		});
	// Authenticated User Course Enrollment ---NON-ADMIN ONLY---
		route.post('/enroll', auth.verify, (req, res) => {
			let token = req.headers.authorization;
			let payload = auth.decode(token);
			let userId = payload.id;
			let isAdmin = payload.isAdmin;
			let subjectId = req.body.courseId;
			let data = {
				userId: userId,
				courseId: subjectId
			};
			if (!isAdmin) {
	        	controller.enroll(data).then(outcome => {
	        		res.send(outcome);
	        	});
	        } else {
	            res.send('Enroll Request Denied! Only non-admin users are permitted to perform this action.'); 
	        }; 
		});

// [SECTION] Route - Retrieve *GET*
	// User Profile
		route.get('/id', auth.verify,(req, res) => {
			let userData = auth.decode(req.headers.authorization);
			let userId = userData.id;
			controller.getProfile(userId).then(outcome => {
				res.send(outcome);
			});
		});

// [SECTION] Route - Update *PUT*
	// Set User as Admin ---ADMIN ONLY---
		route.put('/:userId/set-as-admin', auth.verify,(req, res) => {
			let token = req.headers.authorization;
			let payload = auth.decode(token);
			let isAdmin = payload.isAdmin;
			let id = req.params.userId;
			(isAdmin) ? controller.setAsAdmin(id).then(outcome => res.send(outcome))
			: res.send('Update Request Denied! Admin access is required to perform this action.');
		});

	// Set User as Non-Admin ---ADMIN ONLY---
		route.put('/:userId/set-as-user', auth.verify,(req, res) => {
			let token = req.headers.authorization;
			let isAdmin = auth.decode(token).isAdmin;
			let id = req.params.userId;
			isAdmin ? controller.setAsNonAdmin(id).then(outcome => res.send(outcome))
			: res.send('Update Request Denied! Admin access is required to perform this action.');
		});
		
// [SECTION] Expose Route System
	module.exports = route; 