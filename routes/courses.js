// [SECTION] Dependencies and Modules
	const exp = require('express');
	const controller = require('../controllers/courses');
	const auth = require('../auth');

// [SECTION] Routing Component
	const route = exp.Router();

// [SECTION] Route - Create *POST* ---ADMIN ONLY---
	route.post('/', auth.verify ,(req, res) => {
		let isAdmin = auth.decode(req.headers.authorization).isAdmin;
		let data = {
			course: req.body
		}
		if (isAdmin) {
			controller.addCourse(data).then(outcome => {
				res.send(outcome)
			});
		} else {
			res.send('Create Course Request Denied! Admin access is required to perform this action.');
		}
	});

// [SECTION] Route - Retrieve *GET*
	// Retrieve ALL Courses ---ADMIN ONLY---
		route.get('/all', auth.verify, (req, res) => {
			let token = req.headers.authorization;
			let payload = auth.decode(token);
			let isAdmin = payload.isAdmin;
			isAdmin ? controller.getAllCourse().then(outcome => res.send(outcome)) : res.send('Request Denied! Admin access is required to perform this action.');
		});

	// Retrieve ACTIVE ONLY Courses
		route.get('/', (req, res) => {
			controller.getAllActive().then(outcome => {
				res.send(outcome)
			});
		});

	// Retrieve SINGLE Course
		route.get('/:id', (req, res) => {
			let id = req.params.id;
			controller.getCourse(id).then(outcome => {
				res.send(outcome)
			});
		});

// [SECTION] Route - Update *PUT*  ---ADMIN ONLY---
	// Update Course
		route.put('/:courseId', auth.verify, (req, res) => {
			let params = req.params;
	    	let body = req.body;
	    	if (!auth.decode(req.headers.authorization).isAdmin) {
	    		res.send('Update Request Denied! Admin access is required to perform this action.');
	    	} else {
	    		controller.updateCourse(params, body).then(outcome => {
		    		res.send(outcome);
		    	});
	    	}
	    });

	// Course Archive
		route.put('/:courseId/archive', auth.verify, (req, res) => {
			let token = req.headers.authorization;
			let isAdmin = auth.decode(token).isAdmin;
			let params = req.params;
				(isAdmin)?
					controller.archiveCourse(params).then(outcome => {
			    		res.send(outcome);
			    	})
			    :
			    res.send('Archive Course Request Denied! Admin access is required to perform this action.');
		});

// [SECTION] Route -  Delete *REMOVE* ---ADMIN ONLY---
		route.delete('/:courseId', auth.verify, (req, res) => {
			let token = req.headers.authorization;
			let isAdmin = auth.decode(token).isAdmin;
			let id = req.params.courseId;
			isAdmin ? controller.deleteCourse(id).then(outcome => res.send(outcome)) : res.send('Delete Course Request Denied! Admin access is required to perform this action.');
		});

// [SECTION] Expose Route System
	module.exports = route; 